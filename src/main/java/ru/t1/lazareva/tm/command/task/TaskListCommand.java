package ru.t1.lazareva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.model.Task;
import ru.t1.lazareva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-list";

    @NotNull
    private static final String DESCRIPTION = "Show task list.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final List<Task> tasks = getTaskService().findAll(userId, sort);
        renderTasks(tasks);
    }

}